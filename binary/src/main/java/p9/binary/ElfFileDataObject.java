package p9.binary;

import binparse.elf.ElfBinary;
import java.io.IOException;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.Node;
import org.openide.util.lookup.InstanceContent;

/**
 * Elf object files as recognized by "application/x-elf-object".
 */
public class ElfFileDataObject extends MultiDataObject {
  private static final long serialVersionUID = 1L;

  public ElfFileDataObject (FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
    super(pf, loader);
  }

  @Override protected Node createNodeDelegate () {
    return new BinaryObjectNode(this, new InstanceContent(), ElfBinary.getFactory());
  }

  @Override protected int associateLookup () {
    return 1;
  }
}
