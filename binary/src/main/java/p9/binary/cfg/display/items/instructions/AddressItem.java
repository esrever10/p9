package p9.binary.cfg.display.items.instructions;

import java.awt.Color;

import p9.binary.cfg.InstructionsHelper;
import p9.binary.cfg.display.ColorConfig;
import p9.binary.cfg.display.items.BasicBlockItem;
import p9.binary.cfg.display.items.TextItem;
import p9.binary.cfg.graph.BasicBlock;
import rreil.disassembler.Instruction;
import rreil.lang.RReilAddr;

/**
 * @author Raphael Dümig
 */
public class AddressItem extends TextItem {
  private final RReilAddr address;

  public AddressItem (RReilAddr address) {
    super(addressToString(address));
    this.address = address;
  }

  public RReilAddr getAddress () {
    return address;
  }

  private static String addressToString (RReilAddr address) {
    return String.format("%x.%02x", address.base(), address.offset());
  }

  public String getAddressString () {
    return addressToString(address);
  }

  // use the standard color for addresses
  @Override public void setTextColor (Color color) {
    super.setTextColor(ColorConfig.$Address);
  }

  @Override public String tooltipText () {
    Instruction instruction = ((InstructionItem) getParent()).getInstruction();
    BasicBlock basicBlock = ((BasicBlockItem) getParent().getParent()).getBasicBlock();
    return InstructionsHelper.inoutStatesTooltip(instruction, getViewer().getCfg(), basicBlock);
  }
}
