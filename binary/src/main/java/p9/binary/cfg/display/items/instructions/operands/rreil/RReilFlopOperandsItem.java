package p9.binary.cfg.display.items.instructions.operands.rreil;

import p9.binary.cfg.display.items.ContainerItem;
import p9.binary.cfg.display.items.ListLayout;
import p9.binary.cfg.display.items.TextItem;

/**
 *
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public class RReilFlopOperandsItem extends ContainerItem {

  private final static TextItem separator = new TextItem(",");
  private final static TextItem startItem = new TextItem("(");
  private final static TextItem endItem = new TextItem(")");
  
  static {
    separator.setPadding(PADDING_LEFT, 2);
    separator.setPadding(PADDING_RIGHT, 5);
    startItem.setPadding(PADDING_RIGHT, 2);
    endItem.setPadding(PADDING_LEFT, 2);
  }
  
  public RReilFlopOperandsItem() {
    super(new ListLayout(separator, startItem, endItem));
  }
  
}
