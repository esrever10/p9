package p9.binary.cfg.display.items.instructions.operands.x86;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import p9.binary.cfg.display.ColorConfig;
import p9.binary.cfg.display.items.TextItem;
import p9.binary.cfg.display.items.highlight.ObjectHighlight;

/**
 * @author Raphael Dümig
 */
public class ImmediateItem extends TextItem {
  private final String immediate;

  public ImmediateItem (String immediate) {
    super(immediate);
    this.immediate = immediate;
    this.highlighted = false;
    super.setTextColor(ColorConfig.$Immediate);
  }

  @Override public void setTextColor (Color c) {
    // text color is set in constructor and should not be changed anymore
  }

  @Override public void mouseEntered (MouseEvent me, Point2D relPos) {
    super.mouseEntered(me, relPos);
    getViewer().highlight(new ObjectHighlight(immediate));
  }

  @Override public void mouseExited (MouseEvent me, Point2D relPos) {
    super.mouseExited(me, relPos);
    getViewer().disableHighlight(new ObjectHighlight(immediate));
  }

  @Override public void highlight (ObjectHighlight highlight) {
    if (highlight.contains(immediate)) {
      highlighted = true;
    }
  }

  @Override public void disableHighlight (ObjectHighlight highlighting) {
    if (highlighting.contains(immediate)) {
      highlighted = false;
    }
  }

}
