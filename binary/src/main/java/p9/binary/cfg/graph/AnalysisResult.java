package p9.binary.cfg.graph;

import bindead.analyses.Analysis;
import binparse.rreil.RReilBinary;

/**
 * A container for the results of the analysis.
 *
 * @author Bogdan Mihaila
 */
public class AnalysisResult {
  private final Analysis<?> analysis;
  private final String analyzedFileName;

  public AnalysisResult (Analysis<?> analysis, String analyzedFileName) {
    this.analysis = analysis;
    this.analyzedFileName = analyzedFileName;
  }

  public Analysis<?> getAnalysis () {
    return analysis;
  }

  public String getAnalyzedFileName () {
    return analyzedFileName;
  }

  public boolean hasBinaryFile () {
    return !analysis.getBinaryCode().getBinary().getArchitectureName().equals(RReilBinary.rreilArch);
  }
}
