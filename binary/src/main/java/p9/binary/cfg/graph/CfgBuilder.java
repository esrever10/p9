package p9.binary.cfg.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import prefuse.data.Node;
import rreil.disassembler.Instruction;
import rreil.lang.RReil;
import rreil.lang.RReilAddr;
import rreil.lang.lowlevel.RReilHighLevelToLowLevelWrapper;
import bindead.analyses.BinaryCodeCache;
import bindead.analyses.RReilCodeCache;
import bindead.analyses.algorithms.data.CallString;
import bindead.analyses.algorithms.data.ProgramCtx;
import bindead.analyses.algorithms.data.TransitionSystem;
import bindead.analyses.algorithms.data.TransitionSystem.ProceduralTransitions;
import bindead.domainnetwork.interfaces.ProgramPoint;

import com.google.common.collect.Multimap;

/**
 * Builds CFGs from the results of an analysis.
 *
 * @author Bogdan Mihaila
 */
public class CfgBuilder {
  private final AnalysisResult analysis;
  private final CallString callstring;
  private final BinaryCodeCache binaryCode;
  private final RReilCodeCache rreilCode;
  private final TransitionSystem transitionSystem;

  public CfgBuilder (AnalysisResult analysis, CallString callstring) {
    this.analysis = analysis;
    this.callstring = callstring;
    this.binaryCode = analysis.getAnalysis().getBinaryCode();
    this.rreilCode = analysis.getAnalysis().getRReilCode();
    this.transitionSystem = analysis.getAnalysis().getTransitionSystem();
  }

  public NativeCfg nativeCfg () {
    if (binaryCode == null)
      return null;
    RReilCfg cfg = rreilCfg();
    return new NativeCfgBuilder(cfg).buildNativeCfg();
  }

  public RReilCfg rreilCfg () {
    return new RReilCfgBuilder().buildCfg();
  }

  private class NativeCfgBuilder {
    private final Cfg rreilCfg;
    private final NativeCfg nativeCfg;
    private final Map<Node, Node> nodesMapping;

    /**
     * Assumes that the passed in RREIL CFG is not transformed to
     * contract instructions as basic blocks.
     */
    public NativeCfgBuilder (RReilCfg rreilCfg) {
      this.rreilCfg = rreilCfg;
      nativeCfg = new NativeCfg(analysis, callstring, rreilCfg);
      nodesMapping = new HashMap<>();
    }

    @SuppressWarnings("unchecked") private NativeCfg buildNativeCfg () {
      Map<Node, List<Node>> leaderNodes = new HashMap<>();
      for (Iterator<Node> nodes = rreilCfg.nodes(); nodes.hasNext();) {
        Node node = nodes.next();
        if (!isBeginOfNativeInstruction(node))
          continue;
        leaderNodes.put(node, getNativeSuccessors(node));
      }
      for (Entry<Node, List<Node>> entry : leaderNodes.entrySet()) {
        Node rreilNode = entry.getKey();
        Node node = mapOrFresh(rreilNode);
        node.set(Cfg.$Block, new BasicBlock(getNativeInstruction(rreilNode)));
        for (Node successor : entry.getValue()) {
          nativeCfg.addEdge(node, mapOrFresh(successor));
        }
      }
      return nativeCfg;
    }

    private Instruction getNativeInstruction (Node n) {
      return binaryCode.getInstruction(addressOf(n).base());
    }

    private boolean isBeginOfNativeInstruction (Node n) {
      return addressOf(n).offset() == 0;
    }

    @SuppressWarnings("unchecked") private List<Node> getNativeSuccessors (Node n) {
      List<Node> nativeSuccessors = new ArrayList<>();
      for (Iterator<Node> successors = n.outNeighbors(); successors.hasNext();) {
        Node successor = successors.next();
        if (isBeginOfNativeInstruction(successor))
          nativeSuccessors.add(successor);
        else
          nativeSuccessors.addAll(getNativeSuccessors(successor));
      }
      return nativeSuccessors;
    }

    private RReilAddr addressOf (Node n) {
      return instructionOf(n).address();
    }

    private Instruction instructionOf (Node n) {
      BasicBlock block = blockOf(n);
      assert block.size() == 1;
      return block.get(0);
    }

    private BasicBlock blockOf (Node n) {
      return (BasicBlock) n.get(Cfg.$Block);
    }

    private Node mapOrFresh (Node key) {
      Node value = nodesMapping.get(key);
      if (value == null) {
        value = nativeCfg.addNode();
        nodesMapping.put(key, value);
      }
      return value;
    }

  }

  private class RReilCfgBuilder {
    private final Map<ProgramPoint, Node> nodes = new HashMap<>();
    RReilCfg cfg = new RReilCfg(analysis, callstring);

    public RReilCfg buildCfg () {
      ProceduralTransitions procedure = transitionSystem.getTransitionsFor(callstring);
      for (Entry<ProgramPoint, ProgramPoint> entry : procedure.getLocalTransitions().entries()) {
        try {
          Node source = getOrFresh(entry.getKey());
          Node target = getOrFresh(entry.getValue());
          cfg.addEdge(source, target);
        } catch (IllegalArgumentException _) {
          // when the analysis has been interrupted then an transition was not visited, thus we cannot add an edge here
        }
      }
      Multimap<ProgramPoint, ProgramCtx> calls = procedure.getCalls();
      if (calls.isEmpty())
        return cfg;
      // Fixup call/return sites: For each call-site in the given
      // callstring find all corresponding local successor instructions with the same callstring, that were
      // reached through a return.
      // Basically it adds a local edge between the call-site and the return site in the current procedure.
      // Note that this is an overapproximation as each call-site in this procedure that calls `f` will
      // be connected with the return from `f`. However, only call-sites that called `f` with this call-string
      // will be returned to.
      for (Entry<ProgramPoint, ProgramCtx> call : calls.entries()) {
        ProgramPoint callSite = call.getKey();
        ProgramCtx callTarget = call.getValue();
        ProceduralTransitions callee = transitionSystem.getTransitionsFor(callTarget.getCallString());
        for (ProgramCtx ret : callee.getReturns().values()) {
          if (ret.getCallString().equals(callstring)) {
            Node source = getOrFresh(callSite);
            Node target = getOrFresh(ret);
            cfg.addEdge(source, target);
          }
        }
      }
      return cfg;
    }

    private Node getOrFresh (ProgramPoint key) {
      Node node = nodes.get(key);
      if (node != null)
        return node;
      BasicBlock block = asBasicBlock(key);
      node = cfg.addNode();
      node.set(Cfg.$Block, block);
      nodes.put(key, node);
      return node;
    }

    private BasicBlock asBasicBlock (ProgramPoint key) {
      RReilAddr point = key.getAddress();
      RReil rreil = rreilCode.getInstruction(point);
      return new BasicBlock(new RReilHighLevelToLowLevelWrapper(rreil));
    }

  }

}
