package p9.binary.cfg.graph;

import bindead.analyses.algorithms.data.CallString;

/**
 * @author Bogdan Mihaila
 */
public class RReilCfg extends Cfg {

  public RReilCfg (AnalysisResult analysis, CallString callString) {
    super(analysis, callString);
  }

  @Override public boolean isRReilCfg () {
    return true;
  }
}
