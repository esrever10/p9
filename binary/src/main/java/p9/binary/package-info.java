@TemplateRegistration(
    folder = "Other",
    displayName = "#RReilFiletemplate_displayName",
    content = "RReilFileTemplate.rreil")
@Messages(value = "RReilFiletemplate_displayName=RReil file template")
package p9.binary;

import org.netbeans.api.templates.TemplateRegistration;
import org.openide.util.NbBundle.Messages;

