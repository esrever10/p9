package tools;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.Map;

import javalx.data.Option;
import javalx.data.products.P2;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.openide.util.Exceptions;

import p9.binary.analysis.Analyzer;
import p9.binary.cfg.controller.NativeContextMenu;
import p9.binary.cfg.controller.RReilContextMenu;
import p9.binary.cfg.display.CfgComponent;
import p9.binary.cfg.display.CfgView;
import p9.binary.cfg.graph.AnalysisResult;
import p9.binary.cfg.graph.BasicBlockTransformation;
import p9.binary.cfg.graph.Cfg;
import p9.binary.cfg.graph.CfgBuilder;
import rreil.lang.RReilAddr;
import bindead.analyses.Analysis;
import bindead.analyses.algorithms.data.CallString;
import bindead.analyses.algorithms.data.TransitionSystem;
import bindead.exceptions.CallStringAnalysisException;
import binparse.Binary;
import binparse.BinaryFileFormat;
import binparse.Symbol;

/**
 * Collection of tools to make writing tests easier.
 *
 * @author Bogdan Mihaila
 */
public class TestsHelper {

  public final static String rreilPath = "/rreil/";
  public final static String binariesPath32bit = "/binaries/x86_32/";
  public final static String binariesPath64bit = "/binaries/x86_64/";

  /**
   * Instantiate a binary from a resource file path. Automatically chooses the
   * right binary format (LINUX, Windows, RREIL, etc.).
   *
   * @param resourcePath A file path that is relative to this projects 32 bit
   *          binary examples path.
   * @return The binary
   * @throws IOException if there was an error while trying to access the file
   * @see #binariesPath32bit
   */
  public static Binary getRReilExamplesBinary (String resourcePath) throws IOException {
    String file = TestsHelper.class.getResource(rreilPath + resourcePath).getPath();
    return BinaryFileFormat.getBinary(file);
  }

  /**
   * Instantiate a binary from a resource file path. Automatically chooses the
   * right binary format (LINUX, Windows, RREIL, etc.).
   *
   * @param resourcePath A file path that is relative to this projects 32 bit
   *          binary examples path.
   * @return The binary
   * @throws IOException if there was an error while trying to access the file
   * @see #binariesPath32bit
   */
  public static Binary get32bitExamplesBinary (String resourcePath) throws IOException {
    String file = TestsHelper.class.getResource(binariesPath32bit + resourcePath).getPath();
    return BinaryFileFormat.getBinary(file);
  }

  /**
   * Instantiate a binary from a resource file path. Automatically chooses the
   * right binary format (LINUX, Windows, RREIL, etc.).
   *
   * @param resourcePath A file path that is relative to this projects 64 bit
   *          binary examples path.
   * @return The binary
   * @throws IOException if there was an error while trying to access the file
   * @see #binariesPath64bit
   */
  public static Binary get64bitExamplesBinary (String resourcePath) throws IOException {
    String file = TestsHelper.class.getResource(binariesPath64bit + resourcePath).getPath();
    return BinaryFileFormat.getBinary(file);
  }

  public static AnalysisResult analyze (Binary binary) {
    try {
      Analyzer analyzer = new Analyzer(binary);
      Analysis<?> analysis = analyzer.runAnalysis();
      AnalysisResult result = new AnalysisResult(analysis, binary.getFileName());
      return result;
    } catch (CallStringAnalysisException e) {
      Exceptions.printStackTrace(e);
      String fileName = binary.getFileName();
      AnalysisResult result = new AnalysisResult(e.getAnalysis(), fileName);
      return result;
    }
  }

  /**
   * Analyze a binary from a given filename and return the Native and RREIL CFGs
   * for the given function name.
   */
  public static P2<CfgComponent, CfgComponent> analyzeAndBuildCfgs (String fileName, String functionName)
      throws IOException {
    Binary binary = TestsHelper.get32bitExamplesBinary(fileName);
    AnalysisResult result = TestsHelper.analyze(binary);
    CfgComponent nativeCfg = TestsHelper.buildNativeCfgComponent(result, functionName);
    CfgComponent rreilCfg = TestsHelper.buildRReilCfgComponent(result, functionName);
    return P2.tuple2(nativeCfg, rreilCfg);
  }

  private static CfgView buildNativeCfg (AnalysisResult analysis, CallString callstring) {
    Cfg function = BasicBlockTransformation.run(new CfgBuilder(analysis, callstring).nativeCfg());
    CfgView view = new CfgView(function);
    view.addControlListener(new NativeContextMenu(function, Cfg.getName(analysis, callstring), Cfg.$Block));
    return view;
  }

  private static CfgView buildRReilCfg (AnalysisResult analysis, CallString callstring) {
    Cfg function = BasicBlockTransformation.run(new CfgBuilder(analysis, callstring).rreilCfg());
    CfgView view = new CfgView(function);
    view.addControlListener(new RReilContextMenu(function, Cfg.getName(analysis, callstring), Cfg.$Block));
    return view;
  }

  /**
   * Return a bare bones RREIL CFG component build up from a Prefuse view.
   */
  public static CfgView buildRReilCfg (AnalysisResult result, String functionName) {
    return buildRReilCfg(result, getSomeCallStringForFunction(result, functionName));
  }

  /**
   * Return a bare bones Native CFG component build up from a Prefuse view.
   */
  public static CfgView buildNativeCfg (AnalysisResult result, String functionName) {
    return buildNativeCfg(result, getSomeCallStringForFunction(result, functionName));
  }

  /**
   * Return the RREIL CFG component as used in the GUI tabs.
   */
  public static CfgComponent buildRReilCfgComponent (AnalysisResult result, String functionName) {
    return CfgComponent.buildRReilCfg(result, getSomeCallStringForFunction(result, functionName));
  }

  /**
   * Return the RREIL CFG component as used in the GUI tabs.
   */
  public static CfgComponent buildRReilCfgComponent (AnalysisResult result) {
    return CfgComponent.buildRReilCfg(result, getFirstCallStringForFunction(result));
  }

  /**
   * Return the Native CFG component as used in the GUI tabs.
   */
  public static CfgComponent buildNativeCfgComponent (AnalysisResult result, String functionName) {
    return CfgComponent.buildNativeCfg(result, getSomeCallStringForFunction(result, functionName));
  }

  /**
   * Return the first found call-string in the analysis for a function name.
   */
  public static CallString getFirstCallStringForFunction (AnalysisResult result) {
    Map<CallString, TransitionSystem.ProceduralTransitions> procedures =
      result.getAnalysis().getTransitionSystem().getAllProcedures();
    for (Map.Entry<CallString, TransitionSystem.ProceduralTransitions> entry : procedures.entrySet()) {
      CallString callstring = entry.getKey();
      if (callstring.isRoot())
        continue;
      return callstring; // return the first entry
    }
    throw new IllegalArgumentException("No function found in program.");
  }

  /**
   * Return the first found call-string in the analysis for a function name.
   */
  public static CallString getSomeCallStringForFunction (AnalysisResult result, String functionName) {
    Binary binary = result.getAnalysis().getBinaryCode().getBinary();
    Option<Symbol> symbol = binary.getSymbol(functionName);
    if (symbol.isNone()) {
      throw new IllegalArgumentException("No function with name " + functionName + " found.");
    }
    long functionAddress = symbol.get().getAddress();
    Map<CallString, TransitionSystem.ProceduralTransitions> procedures =
      result.getAnalysis().getTransitionSystem().getAllProcedures();
    for (Map.Entry<CallString, TransitionSystem.ProceduralTransitions> entry : procedures.entrySet()) {
      CallString callstring = entry.getKey();
      if (callstring.isRoot())
        continue;
      CallString.Transition transition = callstring.peek();
      RReilAddr callToCfg = transition.getTarget(); // a procedure is identified by the call entry it has, thus target here
      if (callToCfg.base() == functionAddress) {
        return entry.getKey();
      }
    }
    throw new IllegalArgumentException("No function with name " + functionName + " found.");
  }

  /**
   * Open a new window displaying the given component.
   */
  public static void showWindowWithGraph (final JComponent component) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override public void run () {
        // TODO: old code that also added the search panel. See how to reactivate
//        JComponent body = new JLayeredPane();
//        body.add(cfg, Integer.valueOf(0));
//
//        Box box = new Box(BoxLayout.X_AXIS);
//        box.add(Box.createHorizontalGlue());
//    JComponent searchPanel = searcher.buildSearchPanel();
//    int searchPanelHeight = searchPanel.getPreferredSize().height;
//    box.add(searchPanel);
//    box.setBounds(0, bounds.height - searchPanelHeight - 30, bounds.width, searchPanelHeight);
//        body.add(box, Integer.valueOf(1));
//        body.setBackground(ColorConfig.$Background);
//        body.setForeground(ColorConfig.$Foreground);
//
        JFrame frame = new JFrame("CFG Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Rectangle bounds = new Rectangle(0, 0, 1000, 800);

//        frame.getContentPane().add(body, BorderLayout.CENTER);
//        cfg.setSize(1000, 800); // or setbounds?
        frame.getContentPane().add(component, BorderLayout.CENTER);
        frame.pack();
        frame.setSize(bounds.getSize());
        frame.setVisible(true);
      }
    });
  }
}
