#!/bin/sh
## Requires Apache Maven installed and an internet connection as Maven will download lots of things the first time!
set -e #immediately exit on errors

# build the Prefuse graph library
cd prefuse
# do the normal install but skip tests to be faster
mvn clean install -DskipTests
# build the GUI
cd ..
mvn clean install -DskipTests

cp application/target/p9-*.zip p9.zip